import javax.swing.JOptionPane;

public class CifraMaxima {
	public static int cifraMaxima(int n){
	       int max=0;
	       while(n>0){
	          if(n%10> max){
	          max=n%10;}
	          n/=10;
	      }
	    return max;
	}
	
	public static void main(String[] args){
		final int m=Integer.parseInt(JOptionPane.showInputDialog("Introdu numarul: "));
		       if(m>=0)
		  System.out.println("Cifra maxima a numarului " + m + " este " + cifraMaxima(m));
		else
		  System.out.println("Nu ai introdus un numar natural!");
		  }	
	
}


